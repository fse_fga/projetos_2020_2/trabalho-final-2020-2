# Trabalho Final 2020/2

Este é o trabalho final da disciplina de Fundamentos de Sistemas Embarcados (2020/2). O trabalho final pode ser feito em dupla ou individualmente.

## 1. Objetivos

O objetivo deste trabalho é criar um sistema distribuído de automação residencial utilizando um computador (PC) como sistema computacional central e placas ESP32 como controladores distribuídos, interconectados via Wifi através do protocolo MQTT.

![Figura](/imagens/diagrama_arquitetura.png)

## 2. Componentes do Sistema

O sistema do Servidor Central será composto por:
1. 01 PC conectado à rede (Wifi | Ethernet);
2. Saída de Som para Alarme.

Clientes distribuídos:
1. Dev Kit ESP32;
2. Sensor de Temperatura e Umidade DHT11;
3. Botão (Presente na placa);
4. LED (Presente na Placa).

Broker MQTT (Mosquitto - Núvem)

## 3. Conexões entre os módulos do sistema

1. O servidor Central bem como seus clientes (ESP32) deverão se conectar via Rede Ethernet/Wifi através do protocolo MQTT por meio de um Broker (Ex: Mosquitto, Eclipse, HiveMQ, etc - https://mntolia.com/10-free-public-private-mqtt-brokers-for-testing-prototyping/);
2. Todas as mensagens via MQTT devem estar no formato JSON;
3. **ESP32**: o botão e o LED de serem usados são os dispositivos já integrados no próprio kit de desenvolimento (Botão = GPIO 0 / LED = GPIO 2);
4. **ESP32**: o sensor de temperatura da ESP será o DHT11 (Ligado à GPIO 4);
5. **Servidor Central**: o alarme do deverá ser acionado tocando um arquivo de áudio pela saída de som;

## 4. Requisitos

Os sistema de controle possui os seguintes requisitos.

#### **Servidor Central**:
1. O código do Servidor Central pode ser desenvolvido em Python, C ou C++. A interface gráfica pode ser feita para a Web (Ex: JavaScript);
2. Prover uma interface que mantenha o usuário atualizado sobre o estado de cada dispositivo;
3. Para os dispositivos ESP32 com sensor de Temperatura/Umidade, estas informações deve ser atualizadas na interface a cada 2 segundos;
4. Prover mecanismo para que o usuário possa acionar manualmente todos os dispositivos controláveis (lâmpadas, aparelhos de ar-condicionado, etc.)
5. Prover mecanismo para ligar e desligar o sistema de alarma que, quando ligado, deve tocar um som de alerta ao detectar presenças ou abertura de portas/janelas;
6. Prover suporte para adicionar e remover  clientes  ESP32 cujas funções do botão e led representarão entradas (sensores de presença/porta/janelas ou interruptores) e saídas (acionamento de lâmpadas, tomadas, etc). Cada entrada/saída deve ser nomeada pelo usuário no momento da adição do dispositivo ESP.\
A adição de um novo dispositivo será realizada assim que a ESP se conectar à rede Wifi e enviar uma mensagem inicial de configuração para o tópico fse2020/\<**matricula**\>/dispositivos/\<**ID_do_dispositivo**\>. Neste caso, na tela do Servidor Central deve aparecer o novo dispositivo a ser adicionado onde, através de um comando, deverá ser possível definir:
    1. O cômodo da casa onde o dispositivo estará alocado e que irá definir o nome do tópico onde o mesmo irá publicar suas mensagens (Obs: o nome do tópico não pode conter espaços ou caracteres especiais - preferencialmente manter somente com letras sem acentos e números);
    2. O nome do dispositivo de Entrada e Saída sendo controlados por ele; 
    3. Ao final da configuração, deve ser enviada uma mensagem à ESP informando o nome do tópico/cômodo ao qual ela deverá, a partir deste momento enviar a mensagem com o estado dos dispositivos que controla;
7. A remoção de um cliente pode ocorrer de 2 modos:  
   1. Através da interface gráfica onde o servidor central deverá informar ao dispositivo para que o mesmo volte às configurações de fábrica;
   2. Caso não haja comunicação com o dispositivo remoto, o servidor central deverá remover o dispositivo do cadastro e, na ESP32 deverá haver um modo de reset (pressionando o botão por um tempo determinado).
8. Manter log (Em arqvuio CSV) dos comandos acionados pelos usuários e do acionamento dos alarmes;

Obs. 1: A Matrícula a ser utilizada será a de um dos alunos da dupla de trabalho; 2: O ID_do_dispositivo deve ser o MAC_ADRESS da ESP.

#### **Cliente ESP32**:

Haverão dois tipos de clientes ESP32. Um irá simular um dispositivo conectado permanentemente à energia e outro será um dispositivo operado por baterias (em modo Low Power).

1. O código da ESP32 deve ser desenvolvido em C utilizando o framework ESP-IDF;
2. A ESP32 deverá se conectar via Wifi (com as credenciais sendo definidas em variável de ambiente pelo Menuconfig);
3. Cada cliente ESP32, ao ser iniciado pela primeira vez, deve:  
    3.1 Enviar uma mensagem MQTT de inicialização para o tópico fse2020/\<**matricula**\>/dispositivos/\<**ID_do_dispositivo**\> e se inscrever no mesmo tópico. Esse será o canal de comunicação de retorno do servidor central.   
    3.2 Em seguida, o servidor central irá enviar uma mensagem de retorno (JSON) informando o nome do cômodo ao qual o dispositivo foi associado (que será o nome do tópico ao qual o mesmo irá publicar as informações sobre o estado de seus sensores). O formato deste tópico será: fse2020/\<**matricula**\>/<**comodo**\>.  
    A partir deste momento, cada mudança de estado nos dispositivos controlados pela ESP deve ser publicado nos seguintes tópicos:
    fse2020/\<**matricula**\>/<**comodo**\>/temperatura  <br>
    fse2020/\<**matricula**\>/<**comodo**\>/umidade<br>
    fse2020/\<**matricula**\>/<**comodo**\>/estado<br>

4. Caso a ESP já tenha sido cadastrada no servidor central, deve guardar esta informação em sua memória não volátil (NVS) e, caso seja reiniciada, deve manter o estado anterior e não precisar se cadastrar novamente.
5. Realizar a leitura da temperatura e umidade à partir do sensor DHT11 e enviar seu valor para o servidor central a cada 2 segundos;
6. Monitorar o botão utilizando interrupções e enviar por mensagem push a cada mudança do estado do botão;
7. Acionar o LED (Saída) à partir dos comandos enviados pelo servidor central;

A versão da ESP32 operando por bateria deverá ter as mesmas características de comunicação descritas acima, porém, será utilizada exclusivamente para acionamento de sensores (entradas) operando em modo *low power* e enviando o estado de seu sensor via push sempre que houver uma mudança de estado. Neste caso, não haverá um sensor de temperatura / umidade acoplado.

## 5. README

A descrição de funcionamento bem como as instruções de como rodar todo o ambiente devem ser inscluidas no README dos repositório (Servidor Centrar e ESP32).

## 6. Critérios de Avaliação

A avaliação será realizada seguindo os seguintes critérios:

|   ITEM    |   DETALHE  |   VALOR   |
|-----------|------------|:---------:|
|**Servidor Central**    |       |       |
|**Interface (Estado)**  |   Interface apresentando o estado de cada dispositivo, temperaturas e umidades.  |   1,0   |
|**Interface (Acionamento)** |   Mecanismo para acionamento de dispositivos. |   0,5   |
|**Acionamento do Alarme**   |   Mecanismo de ligar/desligar alarme e acionamento do alarme de acordo com o estado dos sensores. |   0,5   |
|**Log (CSV)**   |   Geração de Log em arquivo CSV.  |   0,5 |
|**Clientes ESP32 - Energia**    |       |       |
|**Leitura de Temperatura / Umidade**    |   Leitura e envio dos valores de temperatura / umidade a cada 2 segundos.  |   1,0   |
|**Acionamento de Dispositivos** |   Correto acionamento e envio do estado da saída de acordo com os comandos do servidor Central.    |   0,5   |
|**Acionamento da Entrada** | Correta detecção e envio do estado da entrada ao servidor central.   |   0,5  |
|**Clientes ESP32 - Bateria**    |       |       |
|**Operação em modo Low Power** | Correta operação da palca em modo *low power* economizando energia.   |   1,0  |
|**Acionamento da Entrada** | Correta detecção e envio do estado da entrada ao servidor central sendo acordado somente no acionamento da GPIO em modo *low power*.   |   0,5  |
|**Geral**    |       |       |
|**Comunicação MQTT**  |   Correta implementação de comunicação entre os dispositivos. |   1,5   |
|**Mecanismo de Cadastramento de Clinetes ESP32**   |   Correta implementação do mecanismo de adição de clientes ESP32 tanto no servidor quanto a configuração do Cliente.  |   1,0 |
|**Qualidade do Código** |   Utilização de boas práticas como o uso de bons nomes, modularização e organização em geral. |   1,5 |
|**Pontuação Extra 1** |   Qualidade e usabilidade acima da média. |   0,5   |
|**Pontuação Extra 2** |   Suporte à OTA na ESP32. |   1,0   |


## 7. Referências

[Biblioteca DHT11 para ESP-IDF ESP32](https://github.com/0nism/esp32-DHT11)

[Eclipse Mosquitto - Broker MQTT](https://mosquitto.org/)
